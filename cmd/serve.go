package main

import (
	"html/template"
	"log"
	"net/http"
	"os"

	"gitlab.com/rlbaker/charleston"
)

var logger *log.Logger

func main() {
	logFile, err := os.OpenFile("server.log", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0660)
	defer logFile.Close()
	if err != nil {
		logger.Fatal(err)
	}

	logger := log.New(logFile, "", log.LstdFlags)

	indexTemplate, err := template.New("index").Parse(IndexTemplate)
	if err != nil {
		logger.Fatal(err)
	}

	priceTemplate, err := template.New("price").Parse(PriceTemplate)
	if err != nil {
		logger.Fatal(err)
	}

	http.HandleFunc("/favicon.ico", http.NotFound)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		logger.Printf("%s %s %s", r.RemoteAddr, r.Method, r.URL)

		if r.Method == "POST" {
			code := r.FormValue("code")
			price := charleston.CodeToPrice(code)

			logger.Printf("Processed: %+v", price)

			priceTemplate.Execute(w, price)
		} else {
			indexTemplate.Execute(w, nil)
		}

	})

	logger.Println("Listening on port 8000")
	logger.Fatal(http.ListenAndServe(":8000", nil))
}

const PriceTemplate = `
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Price Calculator</title>
</head>
<body>
	<form action="#" method="post">
		<input name="code">
		<button>Calcuate</button>
	</form>
	<p>
		Code: {{.Code}}
		<br/>
		Invoice: {{.Invoice}}
		<br/>
		Employee: {{.Employee}}
	</p>
</body>
</html>`

const IndexTemplate = `
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Price Calculator</title>
</head>
<body>
	<form method="post">
		<input name="code">
		<button>Calculate</button>
	</form>
</body>
</html>
`
