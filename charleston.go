package charleston

import (
	"fmt"
	"strconv"
	"strings"
)

// Price contains an invoice price, employee price, and the item code used to calculate them
type Price struct {
	Code     string
	Invoice  string
	Employee string
}

// CodeToPrice converts an item code to an invoice and employee price by substituting characters
// in for digits based on the mapping CHARLESTON -> 1234567890
func CodeToPrice(s string) *Price {
	priceCode := strings.ToUpper(s)

	// Check that all input will map to digits
	for _, c := range priceCode {
		if _, ok := priceMapping[c]; !ok {
			errMsg := fmt.Sprintf("Invalid (%s)", s)
			return &Price{
				Code:     errMsg,
				Invoice:  "ERROR",
				Employee: "ERROR",
			}
		}
	}

	digits := codeToDigits(priceCode)
	cents, err := strconv.ParseFloat(digits, 64)
	if err != nil {
		errMsg := fmt.Sprintf("Invalid (%s)", s)
		return &Price{Code: errMsg}
	}

	invoicePrice := cents / 100
	employeePrice := invoicePrice * 1.1

	return &Price{
		Code:     priceCode,
		Invoice:  formatCurrency(invoicePrice),
		Employee: formatCurrency(employeePrice),
	}
}

var priceMapping = map[rune]rune{
	'C': '1',
	'H': '2',
	'A': '3',
	'R': '4',
	'L': '5',
	'E': '6',
	'S': '7',
	'T': '8',
	'O': '9',
	'N': '0',
}

func codeToDigits(code string) string {
	return strings.Map(func(r rune) rune {
		digit, ok := priceMapping[r]
		if !ok {
			return -1
		}
		return digit

	}, code)
}

func formatCurrency(f float64) string {
	return fmt.Sprintf("$%.2f", f)
}
