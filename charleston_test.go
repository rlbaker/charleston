package charleston

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestValidCodeToPrice(t *testing.T) {
	Convey("Given valid input code 'CLHE'", t, func() {
		inputCode := "CLHE"

		Convey("When converted to a price", func() {
			price := CodeToPrice(inputCode)

			Convey("Then the invoice price should be '$15.26'", func() {
				So(price.Invoice, ShouldEqual, "$15.26")
			})

			Convey("And the employee price should be '$16.79'", func() {
				So(price.Employee, ShouldEqual, "$16.79")
			})
		})
	})

	Convey("Given invalid input code 'CLQE'", t, func() {
		inputCode := "CLQE"

		Convey("When converted to a price", func() {
			price := CodeToPrice(inputCode)

			Convey("Then the output price code should be 'ERROR'", func() {
				So(price.Code, ShouldEqual, "Invalid (CLQE)")
			})

			Convey("Then the invoice price should be 'ERROR'", func() {
				So(price.Invoice, ShouldEqual, "ERROR")
			})

			Convey("And the employee price should be 'ERROR'", func() {
				So(price.Employee, ShouldEqual, "ERROR")
			})
		})
	})

}
